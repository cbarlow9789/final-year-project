angular.module('app.controllers', [])

  .filter('myLimitTo', [function() {
    return function(obj, limit) {
      var keys = Object.keys(obj);
      if (keys.length < 1) {
        return [];
      }

      var ret = new Object,
        count = 0;
      angular.forEach(keys, function(key, arrayIndex) {
        if (count >= limit) {
          return false;
        }
        ret[key] = obj[key];
        count++;
      });
      return ret;
    };
  }])

  .filter('orderByDayNumber', function() {
    return function(items, field, reverse) {
      var filtered = [];
      angular.forEach(items, function(item) {
        filtered.push(item);
      });
      filtered.sort(function(a, b) {
        return (a[field] < b[field] ? 1 : -1);
      });
      if (reverse) filtered.reverse();
      return filtered;
    };
  })

  .filter('myCurrency', ['$filter', function($filter) {
    return function(input) {
      input = parseFloat(input);
      input = input.toFixed(input % 1 === 0 ? 0 : 2);
      return '$' + input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };
  }])

  .controller('loginController', ['$scope', 'LoaderService', '$firebaseArray', '$document', '$state', '$ionicPopup', '$ionicLoading', function($scope, LoaderService, $firebaseArray, $document, $state, $ionicPopup, $ionicLoading) {

    $scope.showAlert = function() {
      var alertPopup = $ionicPopup.alert({
        title: "ERROR",
        template: errorText
      });
    };

    $scope.$on('$ionicView.enter', function() {
      firebase.auth().onAuthStateChanged(function(userLogin) {
        console.log(userLogin);
        if (userLogin) {
          LoaderService.show();
          $state.go('menu.home');

        }
      });
    })

    $scope.doLogin = function(userLogin) {

      console.log(userLogin);

      if ($document[0].getElementById("user_name").value != "" && $document[0].getElementById("user_pass").value != "") {


        firebase.auth().signInWithEmailAndPassword(userLogin.username, userLogin.password).then(function() {
          LoaderService.show();




          var user = firebase.auth().currentUser;

          var name, email, uid;

          if (user.emailVerified) { //check for verification email confirmed by user from the inbox

            console.log("email verified");
            $state.go("menu.home");

            name = user.displayName;
            email = user.email;
            uid = user.uid;
            localStorage.setItem("loggedInUserID", uid);
            localStorage.setItem("loggedInUsername", name);


          } else {

            alert("Email not verified, please check your inbox or spam messages")
            return false;

          } // end check verification email


        }, function(error) {
          // An error happened.
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorCode);
          if (errorCode === 'auth/invalid-email') {
            errorText = ('Enter a valid email.');
            $scope.showAlert();
            return false;
          } else if (errorCode === 'auth/wrong-password') {
            errorText = ('Incorrect password.');
            $scope.showAlert();
            return false;
          } else if (errorCode === 'auth/argument-error') {
            errorText = ('Password must be string.');
            $scope.showAlert();
            return false;
          } else if (errorCode === 'auth/user-not-found') {
            errorText = ('No such user found.');
            $scope.showAlert();
            return false;
          } else if (errorCode === 'auth/too-many-requests') {
            errorText = ('Too many failed login attempts, please try after sometime.');
            $scope.showAlert();
            return false;
          } else if (errorCode === 'auth/network-request-failed') {
            errorText = ('Request timed out, please try again.');
            $scope.showAlert();
            return false;
          } else {
            alert(errorMessage);
            return false;
          }

        });



      } else {

        alert('Please enter email and password');
        return false;

      } //end check client username password


    }; // end $scope.doLogin()



  }])

  .controller('appController', ['$scope', '$firebaseArray', 'CONFIG', '$document', '$state', '$ionicLoading', function($scope, $firebaseArray, CONFIG, $document, $state, $ionicLoading) {

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {

        $document[0].getElementById("username").textContent = localStorage.getItem("loggedInUser");


      } else {
        // No user is signed in.
        $state.go("login");
      }
    });


    $scope.doLogout = function() {

      firebase.auth().signOut().then(function() {
        // Sign-out successful.
        //console.log("Logout successful");
        $ionicLoading.show({
          template: 'Logging Out',
          duration: 3000
        })
        $state.go("login");

      }, function(error) {
        // An error happened.
        console.log(error);
      });

    } // end dologout()



  }])

  .controller('resetController', ['$scope', '$state', '$document', '$firebaseArray', 'CONFIG', function($scope, $state, $document, $firebaseArray, CONFIG) {

    $scope.doResetemail = function(userReset) {



      //console.log(userReset);

      if ($document[0].getElementById("ruser_name").value != "") {


        firebase.auth().sendPasswordResetEmail(userReset.rusername).then(function() {
          // Sign-In successful.
          //console.log("Reset email sent successful");

          $state.go("login");


        }, function(error) {
          // An error happened.
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorCode);


          if (errorCode === 'auth/user-not-found') {
            alert('No user found with provided email.');
            return false;
          } else if (errorCode === 'auth/invalid-email') {
            alert('Email you entered is not complete or invalid.');
            return false;
          }

        });



      } else {

        alert('Please enter registered email to send reset link');
        return false;

      } //end check client username password


    }; // end $scope.doSignup()



  }])



  .controller('signupController', ['$scope', '$state', '$document', '$firebaseArray', 'CONFIG', '$ionicPopup', function($scope, $state, $document, $firebaseArray, CONFIG, $ionicPopup) {

    $scope.doSignup = function(userSignup) {



      //console.log(userSignup);

      if ($document[0].getElementById("cuser_name").value != "" && $document[0].getElementById("cuser_pass").value != "") {


        firebase.auth().createUserWithEmailAndPassword(userSignup.cusername, userSignup.cpassword).then(function() {
          // Sign-In successful.
          //console.log("Signup successful");

          var user = firebase.auth().currentUser;

          user.sendEmailVerification().then(function(result) {

          }, function(error) {
            console.log(error)
          });

          user.updateProfile({
            displayName: userSignup.displayname,
          }).then(function() {

            var alertPopup = $ionicPopup.alert({
              title: 'Success',
              template: 'Account Created'
            });
            alertPopup.then(function(res) {

            });
            $state.go("login");
          }, function(error) {
            // An error happened.
            console.log(error);
          });




        }, function(error) {
          // An error happened.
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorCode);

          if (errorCode === 'auth/weak-password') {
            alert('Password is weak, choose a strong password.');
            return false;
          } else if (errorCode === 'auth/email-already-in-use') {
            alert('Email you entered is already in use.');
            return false;
          }




        });



      } else {

        alert('Please enter email and password');
        return false;

      } //end check client username password


    }; // end $scope.doSignup()



  }])

  .controller('movieDetailsCtrl', ['$location', '$http', '$scope', '$firebaseObject', '$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($location, $http, $scope, $firebaseObject, $ionicPopup) {

      $scope.movieID = $location.search();
      $scope.movieID = parseInt($scope.movieID["movieID"], 10);
      $scope.userID = localStorage.getItem("loggedInUserID");

      console.log($scope.movieID);
      //$scope.exists = false;


      var apiKey = '7baae7b093159f1876fbe91176adcb32';
      var movieDetailEndpoint = "https://api.themoviedb.org/3/movie/";
      var movieID = $scope.movieID;
      $scope.moviename = "";
      $scope.movieData = [];
      $scope.rating = {};
      $scope.rating.max = 5;
      $scope.rating.rate = 0;


      // creating a function for getting the movie list. we use this function when loading next page is needed
      $scope.getMovieDetails = function() {

        var url = movieDetailEndpoint + movieID + '?api_key=' + apiKey; // generating the url
        $scope.movieDetails = [];
        console.log(url);
        $http({

            method: 'GET',
            url: url

          })
          .then(function(response) {
            $scope.poster = response.data.poster_path;
            $scope.moviename = response.data.title;
            $scope.overview = response.data.overview;
            $scope.ratingnumber = response.data.vote_average;
            $scope.runtime = response.data.runtime;
            $scope.imdbID = response.data.imdb_id;
            $scope.releasedate = response.data.release_date;
            $scope.budget = response.data.budget;
            $scope.revenue = response.data.revenue;
            $scope.backdroppath = response.data.backdrop_path;
            $scope.status = response.data.status;

            $scope.backgroundImage = {
              background: 'url(https://image.tmdb.org/t/p/w1066_and_h600_bestv2' + $scope.backdroppath,
              'background-repeat': 'no-repeat',
              'background-size': 'auto 100%',
              'background-position': 'center'
            };

            $scope.movieData = response.data;
            $scope.rating.max = 5;
            $scope.rating.rate = response.data.vote_average / 2;

          })
      }

      $scope.castDetails = [];
      $scope.getCastDetails = function() {
        var url = movieDetailEndpoint + movieID + '/casts' + '?api_key=' + apiKey; // generating the url
        $scope.movieDetails = [];
        console.log(url);
        $http({

            method: 'GET',
            url: url

          })
          .then(function(response) {
            $scope.castDetails = response.data.cast;
          })

      }
      $scope.getMovieDetails();
      $scope.getCastDetails();

      $scope.addToFavs = function() {
        var movieData = $scope.movieData;
        var result;

        var checkRef = firebase.database().ref('users/' + $scope.userID + '/favourites/movies/');
        checkRef.once("value")
          .then(function(snapshot) {

            if (snapshot.child($scope.movieID).exists() == true) {
              result = " already exists in your favourites";
              $scope.showPopup();
            } else {
              result = " has been added to your favourites";
              var ref = firebase.database().ref();
              ref.child('users/' + $scope.userID + '/favourites/movies/' + $scope.movieID).set({
                movieData,
                addedOn: firebase.database.ServerValue.TIMESTAMP
              });
              $scope.showPopup();
              $scope.exist = true;
              $scope.$apply();
            };
          });

        $scope.showPopup = function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Favourite',
            template: $scope.moviename + result
          });
        }
      }

      $scope.removefromFavs = function() {
        var movieData = $scope.movieData;
        var result;

        var checkRef = firebase.database().ref('users/' + $scope.userID + '/favourites/movies/');
        checkRef.once("value")
          .then(function(snapshot) {

            if (snapshot.child($scope.movieID).exists() == false) {
              result = "Movie Doesnt exist";
              $scope.showPopup();
            } else {
              result = " has been removed from your favourites";
              var ref = firebase.database().ref();
              ref.child('users/' + $scope.userID + '/favourites/movies/' + $scope.movieID).remove();
              $scope.showPopup();
              $scope.exist = false;
              $scope.$apply();
            }
          })

        $scope.showPopup = function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Favourite',
            template: $scope.moviename + result
          });
        }
      }


      $scope.checkFavs = function() {
        var checkRef = firebase.database().ref('users/' + $scope.userID + '/favourites/movies/');
        checkRef.once("value")
          .then(function(snapshot) {

            if (snapshot.child($scope.movieID).exists() == true) {
              $scope.exist = true;
              $scope.$apply()
              console.log($scope.exist);
              console.log("in favs");
            } else {
              $scope.exist = false;
              $scope.$apply()
              console.log("not in favs");
            }
          })
      }
      $scope.checkFavs();
    }
  ])

  .controller('searchCtrl', ['LoaderService', '$http', '$scope',
    function(LoaderService, $http, $scope, $state) {
      $scope.searchTerm = "";
      var apiKey = '7baae7b093159f1876fbe91176adcb32';
      var searchMoviesEndpoint = "https://api.themoviedb.org/3/search/movie";
      var page = 0;

      $scope.movieList = [];

      // creating a function for getting the movie list. we use this function when loading next page is needed
      $scope.getMovieList = function() {
        LoaderService.show();

        var url = searchMoviesEndpoint + '?api_key=' + apiKey + '&query=' + $scope.searchTerm; // generating the url
        console.log(url);

        $http({
          method: 'GET',
          url: url
        }).
        success(function(data, status, headers, config) {

          if (status == 200) {
            $scope.movieList.push.apply($scope.movieList, data.results) // appending new movies to current list
          } else {
            console.error('Error happened while getting the movie list.')
          }
          LoaderService.hide();

        }).
        error(function(data, status, headers, config) {
          console.error('Error happened while getting the movie list.')
        });
        $scope.movieList = [];
      }

      $scope.search = function() {
        $scope.searchTerm = $scope.search.term;

        $scope.getMovieList();
      }

    }
  ])



  .controller('moviesCtrl', ['LoaderService', '$scope', '$http',
    function(LoaderService, $scope, $http, $state) {
      LoaderService.show();

      var apiKey = '7baae7b093159f1876fbe91176adcb32';
      var popularMoviesEndpoint = "https://api.themoviedb.org/3/movie/popular";
      var page = 0;

      $scope.movieList = [];

      // creating a function for getting the movie list. we use this function when loading next page is needed
      $scope.getMovieList = function() {

        var url = popularMoviesEndpoint + '?page=' + ++page + '&api_key=' + apiKey; // generating the url

        $http({
          method: 'GET',
          url: url
        }).
        success(function(data, status, headers, config) {

          if (status == 200) {
            page = data.page; // saving current page for pagination
            $scope.movieList.push.apply($scope.movieList, data.results) // appending new movies to current list
            console.log($scope.movieList);
            LoaderService.hide();
          } else {
            console.error('Error happened while getting the movie list.')
            LoaderService.hide();
          }

        }).
        error(function(data, status, headers, config) {
          console.error('Error happened while getting the movie list.')
        });
      }

      $scope.getMovieList(); // calling the function when script is loaded for the first time

    }
  ])

  .controller('myAccountCtrl', ['$scope', '$firebaseArray', 'CONFIG', function($scope, $firebaseArray, CONFIG) {
    // TODO: Show profile data

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log('signed in')
        var user = firebase.auth().currentUser;
        console.log(user);

        email = user.email;
        $scope.email = email;

        username = user.displayName;
        $scope.username = username;

      } else {
        console.log(' No user is signed in')
      }
    });

  }])

  .controller('tvShowsCtrl', ['$scope', '$http',
    function($scope, $http, $state) {

      var apiKey = '7baae7b093159f1876fbe91176adcb32';
      var popularTVEndpoint = "https://api.themoviedb.org/3/tv/popular";
      var page = 0;

      $scope.tvList = [];

      // creating a function for getting the movie list. we use this function when loading next page is needed
      $scope.getTvList = function() {

        var url = popularTVEndpoint + '?page=' + ++page + '&api_key=' + apiKey; // generating the url

        $http({
          method: 'GET',
          url: url
        }).
        success(function(data, status, headers, config) {

          if (status == 200) {
            page = data.page; // saving current page for pagination
            $scope.tvList.push.apply($scope.tvList, data.results) // appending new movies to current list
            console.log($scope.tvList);
          } else {
            console.error('Error happened while getting the tv list.')
          }

        }).
        error(function(data, status, headers, config) {
          console.error('Error happened while getting the tv list.')
        });
      }

      $scope.getTvList(); // calling the function when script is loaded for the first time

    }
  ])

  .controller('tvDetailsCtrl', function($scope, $http, $q, $location, shareData) {

    var tvID = $scope.tvID;
    $scope.tvID = $location.search();
    $scope.tvid = $scope.tvID["tvID"];
    $scope.rating = {};
    $scope.rating.max = 5;
    $scope.rating.rate = 0;



    var apiKey = '7baae7b093159f1876fbe91176adcb32';
    var tvDetailEndpoint = "https://api.themoviedb.org/3/tv/";
    var tvID = $scope.tvid;

    var url = tvDetailEndpoint + tvID + '?api_key=' + apiKey; // generating the url
    console.log(url);
    $scope.seasonDetails = [];
    $scope.episodeData = [];

    var getSeasons = function() {
      var data = "";
      // the only change is right here
      return $http({
          method: 'GET',
          url: url
        })

        .then(function(response) {
          var tvSeasonDetails = [];
          $scope.poster = response.data.poster_path;
          $scope.tvname = response.data.name;
          $scope.overview = response.data.overview;
          $scope.seasons = response.data.number_of_seasons;
          $scope.ratingnumber = response.data.vote_average;
          $scope.genres = response.data.genres;
          for (var i = 0; i < response.data.seasons.length; i++) {
            tvSeasonDetails.push({
              'season_number': response.data.seasons[i].season_number
            })
          }

          $scope.seasonDetails = tvSeasonDetails;
          $scope.rating.max = 5;
          $scope.rating.rate = response.data.vote_average / 2;

          $scope.backdroppath = response.data.backdrop_path;

          $scope.backgroundImage = {
            background: 'url(https://image.tmdb.org/t/p/w1066_and_h600_bestv2' + $scope.backdroppath,
            'background-repeat': 'no-repeat',
            'background-size': 'auto 100%',
            'background-position': 'center'
          };

        })
    }

    getSeasons().then(function() {
      var promises = {};
      angular.forEach($scope.seasonDetails, function(tvSeason, key) {
        promises[key] = $http.get('https://api.themoviedb.org/3/tv/' + tvID + '/season/' + tvSeason.season_number + '?api_key=7baae7b093159f1876fbe91176adcb32');
      });

      $q.all(promises).then(function(episodesBySeasonNumber) {
        angular.forEach(episodesBySeasonNumber, function(response) {
          $scope.episodeData.push(response.data);
          shareData.sendData($scope.episodeData);
        });
      });
    });



  })

  .controller('episodeCtrl', ['$scope', '$location','shareData','$ionicPopup', function($scope, $location, shareData, $ionicPopup) {

    $scope.rating = {};
    $scope.rating.max = 5;
    $scope.rating.rate = 0;

    $scope.userID = localStorage.getItem("loggedInUserID");

    $scope.parameters = $location.search();

    $scope.seasonsData = JSON.parse(angular.toJson(shareData.getData()));
    console.log($scope.seasonsData);

    $scope.tvID = parseInt($scope.parameters["tvID"], 10);
    $scope.season_number = parseInt($scope.parameters["season_number"], 10);
    $scope.episode_number = parseInt($scope.parameters["episode_number"], 10);

    var epno = $scope.episode_number - 1;
    $scope.seasonnumber = $scope.seasonsData[$scope.season_number].name;
    $scope.episodeData = $scope.seasonsData[$scope.season_number].episodes[epno];

    $scope.episode_name = $scope.episodeData.name;
    $scope.air_date = $scope.episodeData.air_date;
    $scope.vote_average = $scope.episodeData.vote_average;
    $scope.still_path = $scope.episodeData.still_path;
    $scope.overview = $scope.episodeData.overview;

    $scope.rating.rate = $scope.vote_average / 2;





    $scope.addToFavs = function() {


      var episodeData = $scope.episodeData;
      var result;

      var checkRef = firebase.database().ref('users/' + $scope.userID + '/favourites/tvShows/' + $scope.tvID + '/' + $scope.season_number );
      checkRef.once("value")
        .then(function(snapshot) {

          if (snapshot.child($scope.episode_number).exists() == true) {
            result = " already exists in your favourites";
            $scope.showPopup();
          } else {
            result = " has been added to your favourites";
            var ref = firebase.database().ref();
            ref.child('users/' + $scope.userID + '/favourites/tvShows/' + $scope.tvID + '/' + $scope.season_number + '/' + $scope.episode_number).set({
              episodeData,
              addedOn: firebase.database.ServerValue.TIMESTAMP
            });
            $scope.showPopup();
            $scope.exist = true;
            $scope.$apply();
          };
        });

        $scope.showPopup = function() {
          var alertPopup = $ionicPopup.alert({
            title: 'Favourite',
            template: $scope.episode_name + result
          });
        }
    }


    $scope.removefromFavs = function() {
      var episodeData = $scope.episodeData;
      var result;

      var checkRef = firebase.database().ref('users/' + $scope.userID + '/favourites/tvShows/' + $scope.tvID + '/' + $scope.season_number );
      checkRef.once("value")
        .then(function(snapshot) {

          if (snapshot.child($scope.episode_number).exists() == false) {
            result = "episode doesnt exist";
            $scope.showPopup();
          } else {
            result = " has been removed from your favourites";
            var ref = firebase.database().ref();
            ref.child('users/' + $scope.userID + '/favourites/tvShows/' + $scope.tvID + '/' + $scope.season_number + $scope.episode_number).remove();
            $scope.showPopup();
            $scope.exist = false;
            $scope.$apply();
          }
        })

      $scope.showPopup = function() {
        var alertPopup = $ionicPopup.alert({
          title: 'Favourite',
          template: $scope.episode_name + result
        });
      }
    }


    $scope.checkFavs = function() {
      var checkRef = firebase.database().ref('users/' + $scope.userID + '/favourites/tvShows/' + $scope.tvID + '/' + $scope.season_number );
      checkRef.once("value")
        .then(function(snapshot) {

          if (snapshot.child($scope.episode_number).exists() == true) {
            $scope.exist = true;
            $scope.$apply()
            console.log($scope.exist);
            console.log("in favs");
          } else {
            $scope.exist = false;
            $scope.$apply()
            console.log("not in favs");
          }
        })
    }
    $scope.checkFavs();

  }])

  .controller('trailersCtrl', ['LoaderService', '$scope', '$http', '$location', function(LoaderService, $scope, $http, $location, myYoutube) {

    $scope.getTrailers = function() {
      LoaderService.show();

      $scope.movieID = $location.search();
      $scope.movieID = parseInt($scope.movieID["movieID"], 10);
      var apiKey = '7baae7b093159f1876fbe91176adcb32';
      $scope.trailers = []

      var url = "https://api.themoviedb.org/3/movie/" + $scope.movieID + "/videos?api_key=" + apiKey;
      $scope.movieDetails = [];
      console.log(url);
      $http({

          method: 'GET',
          url: url

        })
        .then(function(response) {
          $scope.trailers = response.data.results;
          console.log($scope.trailers);
        })
    }

    $scope.getTrailers();

    $scope.$on('$ionicView.loaded', function() {
      LoaderService.hide();
    });
  }])


  .controller('homeCtrl', ['$scope', 'LoaderService', '$firebaseArray', 'CONFIG', '$filter', function($scope, LoaderService, $firebaseArray, CONFIG, $filter) {
    $scope.favourites = [];
    $scope.userID = localStorage.loggedInUserID;

    var checkRef = firebase.database().ref('users/' + $scope.userID + '/favourites/movies/');

    checkRef.once("value")
      .then(function(snapshot) {
        $scope.favourites = snapshot.val();
        $scope.$apply();

        LoaderService.hide();
        console.log($scope.favourites);
      });

  }]);
