angular.module('app.routes', [])

.config(['$stateProvider', '$urlRouterProvider','$ionicConfigProvider',function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.navBar.alignTitle('center');

    $stateProvider

    .state('menu', {
      url: '/menu',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'appController'
    })

    .state('login', {
      cache: false,
      url: '/login',
      templateUrl: "templates/login.html",
      controller: "loginController"
    })

    .state('signup', {
      url: '/signup',
      templateUrl: "templates/signup.html",
      controller: "signupController"
    })

    .state('reset', {
      url: '/reset',
      templateUrl: "templates/resetemail.html",
      controller: "resetController"
    })



    .state('menu.home', {
    cache: false,
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })

  .state('menu.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'searchCtrl'
      }
    }
  })

  .state('menu.myAccount', {
    url: '/account',
    views: {
      'menuContent': {
        templateUrl: 'templates/myAccount.html',
        controller: 'myAccountCtrl'
      }
    }
  })

  .state('menu.movies', {
    url: '/movies',
    views: {
      'menuContent': {
        templateUrl: 'templates/movies.html',
        controller: 'moviesCtrl'
      }
    }
  })

  .state('menu.movieDetails', {
    url: '/movieDetails?movieID',
    views: {
      'menuContent': {
    templateUrl: 'templates/movieDetails.html',
    controller: 'movieDetailsCtrl'
  }
}
  })

  .state('menu.trailers', {
    url: '/movieDetails/trailers?movieID',
    views: {
      'menuContent': {
    templateUrl: 'templates/trailers.html',
    controller: 'trailersCtrl'
  }
}
  })

  .state('menu.tvDetails', {
    url: '/tvDetails?tvID',
    views: {
      'menuContent': {
    templateUrl: 'templates/tvDetails.html',
    controller: 'tvDetailsCtrl'
  }
  }
  })

  .state('menu.tVShows', {
    url: '/tvshows',
    views: {
      'menuContent': {
        templateUrl: 'templates/tVShows.html',
        controller: 'tvShowsCtrl'
      }
    }
  })

  .state('menu.favourites', {
    url: '/favourites',
    views: {
      'menuContent': {
        templateUrl: 'templates/favourites.html',
        controller: 'favouritesCtrl'
      }
    }
  })

  .state('menu.episodeDetails', {
    url: '/episodeDetails?tvID&season_number&episode_number',
    views: {
      'menuContent': {
        templateUrl: 'templates/episodeDetails.html',
        controller: 'episodeCtrl'
      }
    }
  })

    $urlRouterProvider.otherwise('/login');

}])
